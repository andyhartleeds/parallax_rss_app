@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <p>Feed items that have been imported (showing most recently posted first)</p>

                    @if (count($feed_items) > 0)
                        <table>
                            <thead>
                                <th>Title</th>
                                <th>Posted</th>
                                <th>&nbsp;</th>
                            </thead>
                            <tbody>
                                @foreach ($feed_items as $item)
                                    <tr>
                                        <td>{{ $item->title }}</td>
                                        <td>{{ $item->posted_at->format('jS F Y H:i') }}</td>
                                        <td><a href="{{ route('output-apple-news', [$item->id]) }}" download="article.json">Apple News</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $feed_items->links() }}
                    @else
                        <p>There are no feed items to show</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
