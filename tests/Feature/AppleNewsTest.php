<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\FeedItem;

class AppleNewsTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testJSONOutput()
    {
    	$feed_item = new FeedItem;
    	$valid_json = (bool) json_decode($feed_item->outputAppleNewsJSON());
        $this->assertTrue($valid_json);
    }
}
