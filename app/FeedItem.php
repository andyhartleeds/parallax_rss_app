<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedItem extends Model
{
	protected $fillable = ['identifier', 'title', 'author', 'description', 'content', 'posted_at'];
	protected $dates = ['created_at', 'updated_at', 'posted_at'];

	// Slightly hacky right now whilst I get my head around Apple News format...
	public function outputAppleNewsJSON()
	{
		// #TODO - Capture all <img> tags and replace as Apple News formatted instead
		//preg_match_all('<img([\w\W])>', $this->content, $img_matches);
		//dd($img_matches);

		$content = strip_tags($this->content, 'img');

		// Only add byline if we have an author
		if ($this->author)
		{
			$byline = [
				'role'				=>	'byline',
				'layout'			=>	'fullMarginBelowLayout',
				'text'				=>	$this->author,
			];
		}
		else
		{
			$byline = '';
		}

		$data = [
			'title'			=>	$this->title,
			'metadata'		=>	[
				//'thumbnailURL'		=>	'bundle://header.jpg',
				'excerpt'			=>	$this->description,
			],
			'version'		=>	"1.5",
			'identifier'	=>	$this->identifier,
			'language'		=>	'en',
			'layout'		=>	[
				'columns'			=>	10,
				'width'				=>	1024,
				'margin'			=>	85,
				'gutter'			=>	20,
			],
			'documentStyle'	=>	[
				'backgroundColor'	=>	'#F5F9FB',
			],
			'components'	=>	[
				/*
				[
					'role'				=>	'heading1',
					'layout'			=>	'heading1Layout',
					'text'				=>	$this->title,
				],
				[
					'role'				=>	'divider',
					'layout'			=>	'bigDividerLayout',
				],
				*/
				[
					'role'				=>	'title',
					'layout'			=>	'halfMarginBelowLayout',
					'text'				=>	$this->title,
				],
				[
					'role'				=>	'intro',
					'layout'			=>	'halfMarginBelowLayout',
					'text'				=>	$this->description,
				],
				$byline,
				[
					'role'				=>	'body',
					'format'			=>	'html',
					'layout'			=>	'noMarginLayout',
					'textStyle'			=>	'bodyFirstDropCap', // #TODO
					'text'				=>	$content,
				]
			],
			//'textStyles'	=>	[],
			'componentLayouts'	=>	[
				'noMarginLayout'	=>	[
					'columnStart'		=>	0,
					'columnSpan'		=>	7,
				],
				'heading1Layout'	=> [
					'columnStart'		=>	0,
					'columnSpan'		=>	7,
					'margin'			=>	[
						'top'				=>	24,
						'bottom'			=>	3,
					],
				],
				'halfMarginBelowLayout'	=>	[
					'columnStart'		=>	0,
					'columnSpan'		=>	7,
					'margin'			=>	[
						'bottom'			=>	12,
					],
				],
				'fullMarginBelowLayout'	=>	[
					'columnStart'		=>	0,
					'columnSpan'		=>	7,
					'margin'			=>	[
						'bottom'			=>	24,
					],
				],
				'bigDividerLayout'		=>	[
					'ignoreDocumentMargin'	=>	'right',
					'columnStart'			=>	0,
					'columnSpan'			=>	10,
					'margin'				=>	[
						'top'					=>	6,
						'bottom'				=>	6,
					],
				],
			],
			//'componentStyles'	=>	[],
			'componentTextStyles'	=>	[
				'default'	=>	[
					'fontName'		=>	'DINAlternate-Bold',
					'textColor'		=>	'#222222',
					'fontSize'		=>	16,
					'lineHeight'	=>	22,
					'linkStyle'		=>	[
						'textColor'		=>	'#D5B327',
					]
				],
				'default-body'		=>	[
					'fontName'					=>	'IowanOldStyle-Roman',
					'paragraphSpacingBefore'	=>	12,
					'paragraphSpacingAfter'		=>	12,
				],
				'default-title'		=>	[
					'fontName'			=>	'DINAlternate-Bold',
					'fontSize'			=>	42,
					'lineHeight'		=>	44,
					'textColor'			=>	'#53585F',
				],
				'default-intro'		=>	[
					'fontName'			=>	'DINAlternate-Bold',
					'fontSize'			=>	18,
					'lineHeight'		=>	22,
					'textColor'			=>	'#A6AAA9',
				],
				'default-byline'	=>	[
					'fontName'			=>	'DINAlternate-Bold',
					'fontSize'			=>	15,
					'lineHeight'		=>	18,
					'textColor'			=>	'#53585F',
				],
				'default-heading1'	=>	[
					'fontName'			=>	'DINAlternate-Bold',
					'tracking'			=>	0.12,
					'textColor'			=>	'#53585F',
				],
				'default-heading2'	=>	[
					'fontName'			=>	'DINAlternate-Bold',
					'tracking'			=>	0.05,
					'textColor'			=>	'#53585F',
				],
				'bodyFirstDropCap'	=>	[
					'dropCapStyle'		=>	[
						'fontName'			=>	'DINAlternate-Bold',
						'textColor'			=>	'#A6AAA9',
						'numberOfLines'		=>	2,
					],
				],
			],
		];

		return json_encode($data);
	}
}
