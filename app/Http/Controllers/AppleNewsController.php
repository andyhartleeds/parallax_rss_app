<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FeedItem;

class AppleNewsController extends Controller
{
	public function outputJSON($article_id)
	{
		$feed_item = FeedItem::find($article_id);
		return $feed_item->outputAppleNewsJSON();
	}
}
