<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use FeedReader;
use App\FeedItem;

class RSSFeedController extends Controller
{
	const FEED_URL = 'https://api.performfeeds.com/article/zxskw3z3dpaz1jym3qw9v0a7q?_fmt=rss2&_rt=b&_fld=bd,img&_lcl=en';
	const DATE_FORMAT = 'Y-m-d H:i:s';

    public function import()
    {
    	$feed = FeedReader::read(self::FEED_URL);
    	
    	foreach ($feed->get_items() as $item)
    	{
    		// Don't import duplicate feed items!
    		if (!FeedItem::where('identifier', '=', $item->get_id())->exists())
    		{
    			FeedItem::create([
    				'identifier'	=>	$item->get_id(),
    				'title'			=>	$item->get_title(),
    				'author'		=>	$item->get_author(),
    				'description'	=>	$item->get_description(),
    				'content'		=>	$item->get_content(),
    				'posted_at'		=>	$item->get_date(self::DATE_FORMAT),
    			]);

    			echo 'Imported feed item \'' . $item->get_title() . '\' (' . $item->get_id() . ')' . "\n";
    		}
    	}
    }
}
