<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\FeedItem;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feed_items = FeedItem::orderBy('posted_at', 'desc')->paginate(10);

        return view('home', [
            'feed_items'    =>  $feed_items,
        ]);
    }
}
